<?php
require_once("config.php");
$id = $_GET['employeeID'];

$sql = "SELECT * FROM employee WHERE employeeID=?";
$query = $dbConn->prepare($sql);
$query->execute([$id]);
$result = $query->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_address']]);
$result_add = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_primary']]);
$result_pri = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_pri['fk_address']]);
$result_pri_add = $statement->fetch(PDO::FETCH_ASSOC);

if(!$result['fk_contact_secondary']==null){
$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_secondary']]);
$result_sec = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_sec['fk_address']]);
$result_sec_add = $statement->fetch(PDO::FETCH_ASSOC);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/infosheet.css">
</head>
<body>
    <style>
    input {
        color: #000000;
    }
    </style>
    <a href="http://localhost/admin_functions.php"><button>Back to Admin Functions</button></a><br>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information System</h1>
                <h2>Employee Information Form</h2>
            </div>
            <div class="profile-label">
                <h3>Employee Profile</h3>
            </div>
            <form action="crud_update.php" id="data_input_1" method="post">
                <input type="text" name="ID" value="<?php echo $id?>" hidden>
                <div class="profile-form-container">
                    <table class="profile-form">
                        <tr>
                            <td><input type="text" name="empLastName" placeholder="First Name"
                                    value="<?php echo $result['name_last'] ?>" disabled></td>
                            <td><input type="text" name="empMiddleInitial" placeholder="Middle Initial"
                                    value="<?php echo $result['name_middle'] ?>" disabled></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="empFirstName" placeholder="Last Name"
                                    value="<?php echo $result['name_first'] ?>" disabled></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="birthday">Date of Birth</label>
                                <input type="date" name="birthday" id="birthday" value="<?php echo $result['date_birth'] ?>"></td>
                        </tr>
                        <tr>
                            <td> Single
                                <input type="radio" name="maritalStatus" id="maritalStatus" value="S" <?php if($result['marital_status']=="S") echo "checked" ?>>
                                Married
                                <input type="radio" name="maritalStatus" id="maritalStatus" value="M" <?php if($result['marital_status']=="M") echo "checked" ?>></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="empHome" value="<?php echo $result['contact_phone'] ?>"></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="SSN" value="<?php echo $result['SSN'] ?>"></td>
                        </tr>
                        <tr>
                            <td>Home Address: </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="emp_stAdd" value="<?php echo $result_add['street_address'] ?>"></td>
                            <td><input type="text" name="emp_cityAdd" value="<?php echo $result_add['city'] ?>"></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="emp_stateAdd" value="<?php echo $result_add['state'] ?>"></td>
                            <td><input type="text" name="emp_zip" value="<?php echo $result_add['zip_code'] ?>"></td>
                        </tr>
                    </table>
                </div>
                <div class="emergency-label">
                    <h3>Emergency Contact Information</h3>
                </div>
                <div class="primary-container">
                    <div class="primary-label">
                        <h3>Primary Contact</h3>
                        <input type="text" name="pri_firstName" value="<?php echo $result_pri['name_first']?>"><br>
                        <input type="text" name="pri_lastName" value="<?php echo $result_pri['name_last']?>"><br>
                        <input type="text" name="pri_middleInitial" value="<?php echo $result_pri['name_middle']?>"><br>
                        <input type="text" name="pri_relationship" value="<?php echo $result_pri['relation_to_emp']?>"><br>
                        <label for="address">Home Address</label><br>
                        <input type="text" name="primary_stAdd" value="<?php echo $result_pri_add['street_address']?>">&nbsp;
                        <input type="text" name="primary_cityAdd"
                        value="<?php echo $result_pri_add['city']?>"><br>
                        <input type="text" name="primary_stateAdd" value="<?php echo $result_pri_add['state']?>">&nbsp;
                        <input type="text" name="primary_zip"
                        value="<?php echo $result_pri_add['zip_code']?>"><br>
                        <input type="text" name="primHome" value="<?php echo $result_pri['contact_phone']?>"><br>
                        <input type="text" name="primWork" value="<?php echo $result_pri['contact_work_phone']?>"><br>
                    </div>
                </div>
                <script>
                    function toggleCheck(input) {
                        if (input.checked == false)
                            document.getElementById('secondary_form').style.display = 'none';
                        else
                            document.getElementById('secondary_form').style.display = 'block';
                    }
                </script>
                <div class="secondary-container" >
                    <div class="secondary-label">
                        <input type="checkbox" onchange='toggleCheck(this);' name="secondary_toggle"
                            id="secondary_toggle" value="toggle">
                        <h3>Secondary Contact</h3>
                        <div id="secondary_form" style="display:none;">

                            <input type="text" name="sec_firstName" value="<?php echo $result_sec['name_first']?>"><br>
                            <input type="text" name="sec_lastName" value="<?php echo $result_sec['name_last']?>"><br>
                            <input type="text" name="sec_middleInitial" value="<?php echo $result_sec['name_middle']?>"><br>
                            <input type="text" name="sec_relationship" value="<?php echo $result_sec['relation_to_emp']?>"><br>
                            <label for="address">Home Address</label><br>
                            <input type="text" name="sec_stAdd" value="<?php echo $result_sec_add['street_address']?>">&nbsp;
                            <input type="text" name="sec_cityAdd" value="<?php echo $result_sec_add['city']?>"><br>
                            <input type="text" name="sec_stateAdd" value="<?php echo $result_sec_add['state']?>">&nbsp;
                            <input type="text" name="sec_zip" value="<?php echo $result_sec_add['zip_code']?>"><br>
                            <input type="text" name="sec_Home" value="<?php echo $result_sec['contact_phone']?>"><br>
                            <input type="text" name="sec_Work" value="<?php echo $result_sec['contact_work_phone']?>"><br>
                        </div>
                    </div>
                </div>
            </form>
            <script>
                function verify_and_send(){
                    if(true) //verify condition
                        document.getElementById('data_input_1').submit();
                }
            </script>
            <button onclick="verify_and_send()">Save</button>
        </div>
    </div>
</body>
</html>

