<?php
session_start();
require_once('config.php');
$password = $_POST['password'];
$userpassword = password_hash($password, PASSWORD_BCRYPT);
$sql = "UPDATE employee SET auth_password=?, initial_login=? where username=?";

$statement = $dbConn->prepare($sql);

$result = $statement->execute([$userpassword, false, $_SESSION["username"]]);
if($result > 0) {
    $_SESSION["data_gathering_mode"] = true;
    header('Location:http://localhost/index.php');
    }
else
    header('Location:http://localhost/first_log_password.html');
