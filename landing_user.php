<?php
session_start();
require_once('config.php');
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);


$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_address']]);
$result_add = $statement->fetch(PDO::FETCH_ASSOC);


$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_primary']]);
$result_pri = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_secondary']]);
$result_sec = $statement->fetch(PDO::FETCH_ASSOC);
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>
    <link rel="stylesheet" href="./css/landingpage.css">
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <h1 class="label">Hi <?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?></h1>
            <p class="label">Where would you want to go?</p>
            <table class="info" cellspacing="10">
                <tr>
                    <td>
                        <div class="emp-info">
                            <a href="./infosheet.php"><img src="./images/user-edit.png" width="100px" height="100px"><br>
                            Employee Information</a>
                        </div>
                    </td>
                    <td>
                        <div class="united-way-contribution">
                            <a href="./united.php"><img src="./images/contribution.png" width="100px" height="100px"><br>
                            United Way Contribution</a>
                        </div>
                    </td>
                    <td>
                        <div class="united-savings">
                            <a href="us-savings.php"><img src="./images/save.png" width="100px" height="100px"><br>
                            US Savings</a>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="brief-info">
                <h4>Basic User Info</h4>
                <table>
                    <tr>
                        <td>Last Name: </td>
                        <td><input type="text" value="<?php echo $result['name_last'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td>First Name: </td>
                        <td><input type="text" value="<?php echo $result['name_first']." ".$result['name_middle'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td>Home Address: </td>
                        <td><?php echo $result_add['street_address'].", ".$result_add['city'].", ".$result_add['state'].", ".$result_add['zip_code'] ?></td>
                    </tr>
                    <tr>
                        <td>Home Phone: </td>
                        <td><input type="text" value="<?php echo $result['contact_phone'] ?>" disabled></td>
                    </tr>
                </table>
                <h4>Contact Persons</h4>
                <div class="contact-persons">
                    <table>
                        <tr>
                            <td>Primary Contact Person: </td>
                            <td><input type="text" value="<?php echo $result_pri['name_last'].", ".$result_pri['name_first']." ".$result_pri['name_middle'] ?>" disabled></td>
                        </tr>
                        <tr>
                            <td>Secondary Contact Person: </td>
                            <td><input type="text" value="<?php echo $result_sec['name_last'].", ".$result_sec['name_first']." ".$result_sec['name_middle'] ?>" disabled></td>
                        </tr>
                    </table>
                </div>
            </div>
            <br><br>
            <form action="logout.php"><button type="submit">Logout</button></form>
            <br>
            <?php 
            if ($_SESSION["admin_status"])
                echo "<a href='./landing_admin.html'><button>Back to Admin Page</button></a>"
            ?>
        </div>
    </div>
</body>
</html>