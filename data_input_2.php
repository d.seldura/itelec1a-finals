<?php
session_start();
require_once('config.php');
date_default_timezone_set ("Asia/Manila");
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_address']]);
$result_add = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM job_specifics where jobID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_job_specifics']]);
$result_job = $statement->fetch(PDO::FETCH_ASSOC);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/us-savings.css">
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information Systems</h1>
                <h2>U.S. Savings Bond Contribution</h2>
            </div>
            <div class="brief-info"><form action="data_input_2_db_store.php" method="post">
                <table>
                    <tr>
                        <td>
                            <tr>
                                <td>Employee ID:</td>
                                <td><input type="text" value="<?php echo $result['employeeID'] ?>" disabled></td>
                                <td>Employee Department: </td>
                                <td><input type="text" value="<?php echo $result_job['department'] ?>" disabled></td>
                            </tr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <tr>
                                <td>Employee Name: </td>
                                <td><input type="text" value="<?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?>" disabled></td>
                                <td>Employee SSN: </td>
                                <td><input type="text" value="<?php echo $result['SSN'] ?>" disabled></td>
                            </tr>
                        </td>
                    </tr>
                </table>
                <hr>
                <div class="options-wrapper">
                    <div class="options">
                        <label for="bond-denomination">Bond Denomination</label><br>
                        <input type="radio" name="bond-denomination" value="100" onclick="selectBond(this.value)" checked> $100 <br>
                        <input type="radio" name="bond-denomination" value="200" onclick="selectBond(this.value)"> $200 <br>
                        <input type="radio" name="bond-denomination" value="500" onclick="selectBond(this.value)"> $500 <br>
                        <input type="radio" name="bond-denomination" value="1000" onclick="selectBond(this.value)"> $1000 <br>
                    </div>
                    <script>
                        function selectBond(value){
                            document.getElementById('first-payperiod').value=value;
                            document.getElementById('per-payperiod').value=parseFloat(value/12).toFixed(2);
                        }

                        function validate_per(amount){
                            radioChoices = document.getElementsByName("bond-denomination");
                            for (x=0;x<4;x++)
                                if (radioChoices[x].checked) break;
                            if (amount.value)
                                if(amount.value > radioChoices[x].value)
                                    alert("Enter a valid amount");
                        }

                        function disable(target, origin){
                            target.disabled = !target.disabled;
                            origin.value = null; origin.placeholder = "Enter Amount";
                            origin.disabled = !origin.disabled;
                        }
                    </script>
                    <div class="deductions">
                        <label for="deduction-type">Payroll Deduction Type</label><br>
                        <input type="radio" name="deduction-type" value="O" onclick="disable(document.getElementById('per-payperiod'), document.getElementById('first-payperiod'))" checked> One Time Purchase <input type="text" name="first-payperiod" id="first-payperiod" placeholder="Enter Amount" value="100"><br>
                        <label>(Amount to be deducted first payperiod)</label><br>
                        <input type="radio" name="deduction-type" value="R" onclick="disable(document.getElementById('first-payperiod'), document.getElementById('per-payperiod'))"> Regular Deduction <input type="text" name="per-payperiod" id="per-payperiod" onchange="validate_per(this)" placeholder="Enter Amount" disabled><br>
                        <label>(Amount to be deducted per payperiod</label><br>
                    </div>
                </div>
                <hr>
                <div class="beneficiary">
                    <div class="bond-info">
                        <table>
                            <tr>
                                <td>Bond Owner: </td>
                                <td><input type="text" name="bond-owner" value="<?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?>" disabled></td>
                            </tr>
                            <tr>
                                <td>Bond Owner SSN: </td>
                                <td><input type="text" value="<?php echo $result['SSN'] ?>" disabled></td>
                            </tr>
                            <tr>
                                <td>Bond Mailing Address</td>
                                <td><?php echo $result_add['street_address'].", ".$result_add['city'].", ".$result_add['state'].", ".$result_add['zip_code'] ?></td>
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="credentials">
                    <div class="signature-date">
                        <table>
                            <tr>
                                <td><button type="submit">Save</button></form></td>
                                <td><?php echo (new \DateTime())->format('Y-m-d');?></td>
                            </tr>
                            <tr>
                                <td><a href="data_input_3.php"><button>Opt out</button></a></td><td>Date</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>