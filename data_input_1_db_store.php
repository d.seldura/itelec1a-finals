<?php
session_start();
require_once('config.php');
echo var_dump($_POST);


//employee address
$statement = $dbConn->prepare("INSERT INTO address (street_address,city,state,zip_code) VALUES(?,?,?,?)");
$result = $statement->execute([$_POST['emp_stAdd'],$_POST['emp_cityAdd'],$_POST['emp_stateAdd'],$_POST['emp_zip']]);
$fk_address_emp = $dbConn->lastInsertId();

//primary_add
$statement = $dbConn->prepare("INSERT INTO address (street_address,city,state,zip_code) VALUES(?,?,?,?)");
$result = $statement->execute([$_POST['primary_stAdd'],$_POST['primary_cityAdd'],$_POST['primary_stateAdd'],$_POST['primary_zip']]);
$fk_address_pri = $dbConn->lastInsertId();

//other_person_primary
$statement = $dbConn->prepare("INSERT INTO other_person (name_first,name_middle,name_last,contact_phone,contact_work_phone,fk_address,fk_related_to,relation_to_emp,isContact) VALUES(?,?,?,?,?,?,?,?,?)");
$result = $statement->execute([$_POST['pri_firstName'],$_POST['pri_middleInitial'],$_POST['pri_lastName'],$_POST['primHome'],$_POST['primWork'],$fk_address_pri,$_SESSION["employeeID"],$_POST['pri_relationship'],true]);
$fk_other_pri = $dbConn->lastInsertId();

//secondary
if (isset($_POST['secondary_toggle'])){
    $statement = $dbConn->prepare("INSERT INTO address (street_address,city,state,zip_code) VALUES(?,?,?,?)");
    $result = $statement->execute([$_POST['sec_stAdd'],$_POST['sec_cityAdd'],$_POST['sec_stateAdd'],$_POST['sec_zip']]);
    $fk_address_sec = $dbConn->lastInsertId();

    $statement = $dbConn->prepare("INSERT INTO other_person (name_first,name_middle,name_last,contact_phone,contact_work_phone,fk_address,fk_related_to,relation_to_emp,isContact) VALUES(?,?,?,?,?,?,?,?,?)");
    $result = $statement->execute([$_POST['sec_firstName'],$_POST['sec_middleInitial'],$_POST['sec_lastName'],$_POST['sec_Home'],$_POST['sec_Work'],$fk_address_sec,$_SESSION["employeeID"],$_POST['sec_relationship'],true]);
    $fk_other_sec = $dbConn->lastInsertId();
}
else 
    $fk_other_sec = null;

//employee
$statement = $dbConn->prepare("UPDATE employee SET date_birth = ?,marital_status = ?,fk_address = ?,contact_phone=?,SSN=?,fk_contact_primary=?, fk_contact_secondary=? WHERE employeeID=?");
$result = $statement->execute([$_POST['birthday'],$_POST['maritalStatus'],$fk_address_emp,$_POST['empHome'],$_POST['SSN'],$fk_other_pri,$fk_other_sec,$_SESSION['employeeID']]);

header('Location:http://localhost/data_input_2.php');