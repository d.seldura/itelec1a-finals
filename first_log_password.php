<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/login.css">
</head>

<body>
    <div class="container">
        <form id="password_sumbit" action="_changepass.php" method="post">
            <div class="title-container">
                <h1>Change Password</h1>
            </div>
            <div class="textbox">
                <img src="./images/user.png">
                <input type="text" name="username" id="username" placeholder="Username"
                    value="<?php echo $_SESSION["username"] ?>" readonly>
            </div>
            <div class="textbox">
                <img src="./images/lock.png">
                <input type="password" name="password" id="password" placeholder="Password">
            </div>
            <div class="textbox">
                <img src="./images/lock.png">
                <input type="password" name="pass_verify" id="pass_verify" placeholder="Verify your password">
            </div>
        </form>
        <script>
            alert("Please change your password");
        </script>
        <script>
            function submit() {
                if (document.getElementById("password").value === document.getElementById("pass_verify").value)
                    document.getElementById("password_sumbit").submit();
                else {
                    alert("passwords don't match!");
                    document.getElementById("password_sumbit").reset();
                }
            }
        </script>
        <button class="btn" onclick="submit()">Log in</button>
    </div>
</body>

</html>