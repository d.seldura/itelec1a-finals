    <?php
    require_once("config.php");
    $result = $dbConn->query("SELECT * FROM users ORDER BY id DESC;");
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>
</head>
<body>
    <a href="add.html">Add New Data</a><br><br>
    <table width="80%" border="0">
        <tr bgcolor='#CCCCCC'>
            <td>Name</td>
            <td>Age</td>
            <td>Email</td>
        </tr>
        <?php
        while ($row = $result->fetch(PDO::FETCH_ASSOC)){
            echo "<tr>";
            echo "<td>".$row["name"]."</td>";
            echo "<td>".$row["age"]."</td>";
            echo "<td>".$row["email"]."</td>";
            echo "<td><a href=\"edit.php?id=$row[id]\">Edit</a> | <a href=\"delete.php?id=$row[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td></tr>";
        }
        ?>
    </table>
</body>
</html>
