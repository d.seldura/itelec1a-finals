<?php
session_start();
if (!isset($_SESSION["login-status-admin"])){
    $_SESSION["login-status-admin"] = false;
}
if ($_SESSION["login-status-admin"]){
header('Location:http://localhost/admin/adminPanel.html');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems - Admin Panel</title>
    <link rel="stylesheet" href="../css/login.css">
</head>
<body>
    <div class="container">
        <form action="./scripts/login.php" method="post">
        <div class="title-container">
            <h1>Administrative Page -  Login</h1>
        </div>
        <div class="textbox">
            <img src="http://localhost/images/user.png">
            <input type="text" name="username" id="username" placeholder="Username">
        </div>
        <div class="textbox">
            <img src="http://localhost/images/user.png">
            <input type="password" name="password" id="password" placeholder="Password">
        </div>
            <input class="btn" type="submit" value="Log in">
        </form>
    </div>
</body>
</html>