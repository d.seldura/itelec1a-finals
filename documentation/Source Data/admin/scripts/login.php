<?php
session_start();
require_once('config.php');

$searchsql = "SELECT * from admin_accounts where username = ?";

$username = $_POST['username'];
$password = $_POST['password'];

$prepared = $dbConn->prepare($searchsql);
$prepared->execute(array($username));

$result = $prepared->fetch(PDO::FETCH_ASSOC);

if($result)
    if(password_verify($password,$result['password']))
    {
        $_SESSION["username"] = $username;
        $_SESSION["login-status-admin"] = true;
        header('Location:http://localhost/admin/adminPanel.html');
    }
    else
        header('Location:http://localhost/admin/index.html');
else
    header('Location:http://localhost/admin/index.html');
