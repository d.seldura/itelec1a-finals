create database finals_web;
use finals_web;

CREATE TABLE address (
addressID INT(6) AUTO_INCREMENT PRIMARY KEY,
street_address VARCHAR(256),
city VARCHAR(32),
state VARCHAR(32),
zip_code INT(10)
);

CREATE TABLE work_site (
siteID INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
site_name VARCHAR(32),
building VARCHAR(4),
room VARCHAR(4),
mail_stop VARCHAR(4)
);

CREATE TABLE job_specifics (
jobID INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(64),
department VARCHAR(64),
fk_dept_manager INT(5),
fk_work_site INT(5)
);

CREATE TABLE pay_details (
payID INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
hourly_rate FLOAT(6),
annual_rate FLOAT(9),
scheduled_hours INT(2)
);

CREATE TABLE deductions_savings_bond (
bondID INT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
type CHAR(1),
bond_value INT(4),
deduction_amount FLOAT(7),
fk_beneficiary INT(5),
creation_date DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE deductions_united_way (
united_way_id INT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
type CHAR(1),
deduction_amount FLOAT(7),
creation_date DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE activity_log (
logID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
author_id INT(5),
recipient_id INT(5),
activity_code CHAR(1),
reason VARCHAR(256),
date_of_activity DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE other_person (
personID INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
name_first VARCHAR(32),
name_middle VARCHAR(32),
name_last VARCHAR(32),
contact_phone VARCHAR(16),
contact_work_phone VARCHAR(16),
fk_address INT(6),
fk_related_to INT(5),
relation_to_emp VARCHAR(16),
SSN VARCHAR(9),
isContact INT(1),
isBeneficiary INT(1)
);

CREATE TABLE employee (
employeeID INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(16),
name_first VARCHAR(32),
name_middle VARCHAR(32),
name_last VARCHAR(32),
date_birth VARCHAR(10),
date_hire DATETIME DEFAULT CURRENT_TIMESTAMP,
marital_status CHAR(1),
employment_status CHAR(1),
fk_address INT(6),
contact_phone VARCHAR(16),
contact_work_phone VARCHAR(16),
SSN VARCHAR(9),
fk_job_specifics INT(5),
fk_contact_primary INT(5),
fk_contact_secondary INT(5),
fk_pay_details INT(5),
fk_deduction_bond INT(6),
fk_deduction_uw INT(6),
fk_latest_emp_activity INT(10),
auth_password VARCHAR(256),
initial_login INT(1),
is_admin INT(1),
last_modified DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

ALTER TABLE employee
ADD CONSTRAINT fk_address
FOREIGN KEY (fk_address) REFERENCES address(addressID);
ALTER TABLE employee
ADD CONSTRAINT fk_job_specifics
FOREIGN KEY (fk_job_specifics) REFERENCES job_specifics(jobID);
ALTER TABLE employee
ADD CONSTRAINT fk_contact_primary
FOREIGN KEY (fk_contact_primary) REFERENCES other_person(personID);
ALTER TABLE employee
ADD CONSTRAINT fk_contact_secondary
FOREIGN KEY (fk_contact_secondary) REFERENCES other_person(personID);
ALTER TABLE employee
ADD CONSTRAINT fk_pay_details
FOREIGN KEY (fk_pay_details) REFERENCES pay_details(payID);
ALTER TABLE employee
ADD CONSTRAINT fk_deduction_bond
FOREIGN KEY (fk_deduction_bond) REFERENCES deductions_savings_bond(bondID);
ALTER TABLE employee
ADD CONSTRAINT fk_deduction_uw
FOREIGN KEY (fk_deduction_uw) REFERENCES deductions_united_way(united_way_ID);
ALTER TABLE employee
ADD CONSTRAINT fk_latest_emp_activity
FOREIGN KEY (fk_latest_emp_activity) REFERENCES activity_log(logID);

ALTER TABLE job_specifics
ADD CONSTRAINT fk_dept_manager
FOREIGN KEY (fk_dept_manager) REFERENCES employee(employeeID);
ALTER TABLE job_specifics
ADD CONSTRAINT fk_work_site
FOREIGN KEY (fk_work_site) REFERENCES work_site(siteID);

ALTER TABLE deductions_savings_bond
ADD CONSTRAINT fk_beneficiary
FOREIGN KEY (fk_beneficiary) REFERENCES other_person(personID);

ALTER TABLE activity_log
ADD CONSTRAINT recipient_id
FOREIGN KEY (recipient_id) REFERENCES employee(employeeID);
ALTER TABLE activity_log
ADD CONSTRAINT author_id
FOREIGN KEY (author_id) REFERENCES employee(employeeID);

ALTER TABLE other_person
ADD CONSTRAINT fk_address_other
FOREIGN KEY (fk_address) REFERENCES address(addressID);
ALTER TABLE other_person
ADD CONSTRAINT fk_related_to
FOREIGN KEY (fk_related_to) REFERENCES employee(employeeID);

INSERT INTO employee (username,auth_password, initial_login, is_admin) values ("ADMIN", "$2y$10$7nU4eBVGN9XGVURbUXIcCOjxwIKg..evhT9s5Z3JlIRxbIwyBdWri", false, true);