<?php
session_start();
require_once('config.php');

$username = $_POST['username'];
$userpassword = $_POST['password'];
$searchsql = "SELECT * from employee where username = ?";
$prepared = $dbConn->prepare($searchsql);
$prepared->execute(array($username));
$result = $prepared->fetch(PDO::FETCH_ASSOC);

if($result)
    if(password_verify($userpassword,$result['auth_password']))
    {
        $_SESSION["username"] = $username;
        $_SESSION["employeeID"] = $result[employeeID];
        $_SESSION["login_status"] = true;
        if($result['is_admin'])
            $_SESSION["admin_status"] = true;
        else
            $_SESSION["admin_status"] = false;
        if($result['initial_login'])
            header('Location:http://localhost/first_log_password.php');
        else
            header('Location:http://localhost/index.php');
   }
    else
        header('Location:http://localhost/login.html');
else
    header('Location:http://localhost/login.html');
