<?php
session_start();
require_once('config.php');
date_default_timezone_set ("Asia/Manila");
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_address']]);
$result_add = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM job_specifics where jobID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_job_specifics']]);
$result_job = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM deductions_savings_bond WHERE bondID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_deduction_bond']]);
$result_deduction_bond = $statement->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/us-savings.css">
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information Systems</h1>
                <h2>U.S. Savings Bond Contribution</h2>
            </div>
            <div class="brief-info">
                <table>
                    <tr>
                        <td>
                            <tr>
                                <td>Employee ID:</td>
                                <td><input type="text" value="<?php echo $result['employeeID'] ?>" disabled></td>
                                <td>Employee Department: </td>
                                <td><input type="text" value="<?php echo $result_job['department'] ?>" disabled></td>
                            </tr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <tr>
                                <td>Employee Name: </td>
                                <td><input type="text" value="<?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?>" disabled></td>
                                <td>Employee SSN: </td>
                                <td><input type="text" value="<?php echo $result['SSN'] ?>" disabled></td>
                            </tr>
                        </td>
                    </tr>
                </table>
                <hr>
                <div class="options-wrapper">
                    <div class="options">
                        <label for="bond-denomination">Bond Denomination</label><br>
                        <h1>$<?php echo $result_deduction_bond['bond_value']?></h1>
                    </div>
                    <div class="deductions">
                        <label for="deduction-type">Payroll Deduction Type</label><br>
                        <?php 
                            if($result_deduction_bond['type'] == 'O')
                                echo "<h1>One Time Purchase</h1>";
                            else
                                echo "Regular Deduction";
                        ?>
                        <label>Deduction Amount</label><br>
                        <h1>$<?php echo $result_deduction_bond['deduction_amount'] ?></h1>
                    </div>
                </div>
                <hr>
                <div class="beneficiary">
                    <div class="bond-info">
                        <table>
                            <tr>
                                <td>Bond Owner: </td>
                                <td><input type="text" name="bond-owner" value="<?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?>" disabled></td>
                            </tr>
                            <tr>
                                <td>Bond Owner SSN: </td>
                                <td><input type="text" value="<?php echo $result['SSN'] ?>" disabled></td>
                            </tr>
                            <tr>
                                <td>Bond Mailing Address</td>
                                <td><?php echo $result_add['street_address'].", ".$result_add['city'].", ".$result_add['state'].", ".$result_add['zip_code'] ?></td>
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <a href="./landing_user.php"><button>Home</button></a>
            </div>
        </div>
    </div>
</body>
</html>