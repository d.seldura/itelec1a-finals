<?php
session_start();
if(!$_SESSION["login_status"])
    header('Location:http://localhost/login.php');
else if($_SESSION["data_gathering_mode"])
    header('Location:http://localhost/data_input_1.php');
else if ($_SESSION["admin_status"])
    header('Location:http://localhost/landing_admin.html');
    else
    header('Location:http://localhost/landing_user.html');
