<?php
session_start();
require_once('config.php');
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/infosheet.css">
    <style>
        input {
            color: #000000;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information System</h1>
                <h2>Employee Information Form</h2>
            </div>
            <div class="profile-label">
                <h3>Employee Profile</h3>
            </div>
            <form action="data_input_1_db_store.php" id="data_input_1" method="post">
                <div class="profile-form-container">
                    <table class="profile-form">
                        <tr>
                            <td><input type="text" name="empLastName" placeholder="First Name"
                                    value="<?php echo $result['name_last'] ?>" disabled></td>
                            <td><input type="text" name="empMiddleInitial" placeholder="Middle Initial"
                                    value="<?php echo $result['name_middle'] ?>" disabled></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="empFirstName" placeholder="Last Name"
                                    value="<?php echo $result['name_first'] ?>" disabled></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="birthday">Date of Birth</label>
                                <input type="date" name="birthday" id="birthday" placeholder="Birth Date"></td>
                        </tr>
                        <tr>
                            <td> Single
                                <input type="radio" name="maritalStatus" id="maritalStatus" value="S" checked>
                                Married
                                <input type="radio" name="maritalStatus" id="maritalStatus" value="M"></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="empHome" placeholder="Home Phone"></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="SSN" placeholder="Social Security Number"></td>
                        </tr>
                        <tr>
                            <td>Home Address: </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="emp_stAdd" placeholder="Street"></td>
                            <td><input type="text" name="emp_cityAdd" placeholder="City"></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="emp_stateAdd" placeholder="State"></td>
                            <td><input type="text" name="emp_zip" placeholder="Zip Code"></td>
                        </tr>
                    </table>
                </div>
                <div class="emergency-label">
                    <h3>Emergency Contact Information</h3>
                </div>
                <div class="primary-container">
                    <div class="primary-label">
                        <h3>Primary Contact</h3>
                        <input type="text" name="pri_firstName" placeholder="First Name"><br>
                        <input type="text" name="pri_lastName" placeholder="Last Name"><br>
                        <input type="text" name="pri_middleInitial" placeholder="Middle Initial"><br>
                        <input type="text" name="pri_relationship" placeholder="Relationship"><br>
                        <label for="address">Home Address</label><br>
                        <input type="text" name="primary_stAdd" placeholder="Street">&nbsp;<input type="text" name="primary_cityAdd"
                            placeholder="City"><br>
                        <input type="text" name="primary_stateAdd" placeholder="State">&nbsp;<input type="text" name="primary_zip"
                            placeholder="Zip Code"><br>
                        <input type="text" name="primHome" placeholder="Home Phone"><br>
                        <input type="text" name="primWork" placeholder="Work Phone"><br>
                    </div>
                </div>
                <script>
                    function toggleCheck(input) {
                        if (input.checked == false)
                            document.getElementById('secondary_form').style.display = 'none';
                        else
                            document.getElementById('secondary_form').style.display = 'block';
                    }
                </script>
                <div class="secondary-container">
                    <div class="secondary-label">
                        <input type="checkbox" onchange='toggleCheck(this);' name="secondary_toggle"
                            id="secondary_toggle" value="toggle">
                        <h3>Secondary Contact</h3>
                        <div id="secondary_form" style="display:none;">

                            <input type="text" name="sec_firstName" placeholder="First Name"><br>
                            <input type="text" name="sec_lastName" placeholder="Last Name"><br>
                            <input type="text" name="sec_middleInitial" placeholder="Middle Initial"><br>
                            <input type="text" name="sec_relationship" placeholder="Relationship"><br>
                            <label for="address">Home Address</label><br>
                            <input type="text" name="sec_stAdd" placeholder="Street">&nbsp;<input type="text" name="sec_cityAdd"
                                placeholder="City"><br>
                            <input type="text" name="sec_stateAdd" placeholder="State">&nbsp;<input type="text" name="sec_zip"
                                placeholder="Zip Code"><br>
                            <input type="text" name="sec_Home" placeholder="Home Phone"><br>
                            <input type="text" name="sec_Work" placeholder="Work Phone"><br>
                        </div>
                    </div>
                </div>
            </form>
            <script>
                function verify_and_send(){
                    if(true) //verify condition
                        document.getElementById('data_input_1').submit();
                }
            </script>
            <button onclick="verify_and_send()">Save</button>
        </div>
    </div>
</body>

</html>