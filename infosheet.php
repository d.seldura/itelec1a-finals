<?php
session_start();
require_once('config.php');
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);


$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_address']]);
$result_add = $statement->fetch(PDO::FETCH_ASSOC);


$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_primary']]);
$result_pri = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_pri['fk_address']]);
$result_pri_add = $statement->fetch(PDO::FETCH_ASSOC);

if(!$result['fk_contact_secondary']==null){
$sql = "SELECT * FROM other_person where personID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_contact_secondary']]);
$result_sec = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM address where addressID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_sec['fk_address']]);
$result_sec_add = $statement->fetch(PDO::FETCH_ASSOC);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/infosheet.css">
</head>
<body>
    <style>
    input {
        color: #000000;
    }
    </style>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information System</h1>
                <h2>Employee Information Form</h2>
            </div>
            <div class="profile-label">
                <h3>Employee Profile</h3>
            </div>
            <div class="profile-form-container">
                <a href="http://localhost/landing_user.php"><button>Back</button></a><br>
                <label for="date">Date hired: </label>
                <input type="text" id="date" value="<?php echo $result['date_hire'] ?>" disabled>
                <table class="profile-form">
                    <tr>
                        <td><input type="text" id="empLastName" value="<?php echo $result['name_last'] ?>" disabled></td>
                        <td><input type="text" id="empMiddleInitial" value="<?php echo $result['name_middle'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="empFirstName" value="<?php echo $result['name_first'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="birthday" value="<?php echo $result['date_birth'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td>Married<input type="checkbox" id="maritalStatus" <?php if($result['marital_status']=="M") echo " checked" ?> disabled>Single<input type="checkbox" name="maritalStatus" id="maritalStatus" value="S"<?php if($result['marital_status']=="S") echo " checked" ?> disabled></td>
                    </tr>
                    <tr>
                        <td>Contact Number<input type="text" id="empHome" value="<?php echo $result['contact_phone'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td>SSN<input type="text" id="ssn" value="<?php echo $result['SSN'] ?>" disabled></td>
                    </tr>
                    <tr>
                        <td>Home Address: </td>
                    </tr>
                    <tr>
                        <td><?php echo $result_add['street_address'].", ".$result_add['city'].", ".$result_add['state'].", ".$result_add['zip_code'] ?></td>
                    </tr>
                </table>
            </div>
            <div class="emergency-label">
                <h3>Emergency Contact Information</h3>
            </div>
            <div class="primary-container">
                <div class="primary-label">
                    <h3>Primary Contact</h3>
                </div>
                <form action="primary-contact">
                    <input type="text" id="firstName" value="<?php echo $result_pri['name_first']?>" disabled><br>
                    <input type="text" id="lastName" value="<?php echo $result_pri['name_last']?>" disabled><br>
                    <input type="text" id="middleInitial" value="<?php echo $result_pri['name_middle']?>" disabled><br>
                    <input type="text" id="relationship" value="<?php echo $result_pri['relation_to_emp']?>" disabled><br>
                    <label for="address">Home Address</label><br>
                    <input type="text" id="stAdd" value="<?php echo $result_pri_add['street_address']?>" disabled>&nbsp;
                    <input type="text" id="cityAdd" value="<?php echo $result_pri_add['city']?>" disabled><br>
                    <input type="text" id="stateAdd" value="<?php echo $result_pri_add['state']?>" disabled>&nbsp;
                    <input type="text" id="zip" value="<?php echo $result_pri_add['zip_code']?>" disabled><br>
                    <input type="text" id="primHome" value="<?php echo $result_pri['contact_phone']?>" disabled><br>
                    <input type="text" id="primWork" value="<?php echo $result_pri['contact_work_phone']?>" disabled><br>
                </form>
            </div>
            <div class="secondary-container" <?php if($result['fk_contact_secondary']==null) echo "hidden" ?>>
                    <div class="secondary-label">
                        <h3>Secondary Contact</h3>
                    </div>
                    <form action="secondary-contact">
                        <input type="text" id="firstName" value="<?php echo $result_sec['name_first']?>" disabled><br>
                        <input type="text" id="lastName" value="<?php echo $result_sec['name_last']?>" disabled><br>
                        <input type="text" id="middleInitial" value="<?php echo $result_sec['name_middle']?>" disabled><br>
                        <input type="text" id="relationship" value="<?php echo $result_sec['relation_to_emp']?>" disabled><br>
                        <label for="address">Home Address</label><br>
                        <input type="text" id="stAdd" value="<?php echo $result_sec_add['street_address']?>" disabled>&nbsp;
                        <input type="text" id="cityAdd" value="<?php echo $result_sec_add['city']?>" disabled><br>
                        <input type="text" id="stateAdd" value="<?php echo $result_sec_add['state']?>" disabled>&nbsp;
                        <input type="text" id="zip" value="<?php echo $result_sec_add['zip_code']?>" disabled><br>
                        <input type="text" id="secHome" value="<?php echo $result_sec['contact_phone']?>" disabled><br>
                        <input type="text" id="secWork" value="<?php echo $result_sec['contact_work_phone']?>" disabled><br>
                    </form>
            </div>
        </div>
    </div>
</body>
</html>