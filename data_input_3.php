<?php
session_start();
require_once('config.php');
date_default_timezone_set ("Asia/Manila");
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM job_specifics where jobID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_job_specifics']]);
$result_job = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM employee where employeeID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_job['fk_dept_manager']]);
$result_manager = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM pay_details where payID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_pay_details']]);
$result_pay = $statement->fetch(PDO::FETCH_ASSOC);
$pay = $result_pay['hourly_rate']*$result_pay['scheduled_hours']*4;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/united.css">
</head>
<body>
    <div class="container">
        <form action="data_input_3_db_store.php" id="myForm" method="post">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information Systems</h1>
                <h3>Employee United Way Contribution Form</h3>
            </div>
            <div class="employee-info">
                <table class="info-table">
                    <tr>
                        <td>Employee ID: </td>
                        <td><?php echo $result['employeeID'] ?></td>
                        <td>Employee Dept: </td>
                        <td><?php echo $result_job['department'] ?></td>
                    </tr>
                    <tr>
                        <td>Employee Name: </td>
                        <td><?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?></td>
                        <td>Employee Manager: </td>
                        <td><?php echo $result_manager['name_last'].", ".$result_manager['name_first']." ".$result_manager['name_middle']?></td>
                    </tr>
                </table>
            </div>
            <div class="label">
                <h3>My contribution for the calendar year  of (year) is: </h3>
            </div>
            <div class="interactive-form">
                <table class="form">
                    <script> 
                            function disable(target, origin){
                                if (target.disabled && origin.disabled){
                                    target.disabled = false;
                                    target.placeholder = "Enter Amount";
                                    origin.placeholder = "Enter Amount";
                                    }
                                else{
                                    target.disabled = !target.disabled;
                                    origin.value = null; origin.placeholder = "Enter Amount";
                                    origin.disabled = !origin.disabled;
                                }
                            }
                            function disableOther(){
                                document.getElementById('otg_amt').disabled = true;
                                document.getElementById('other_amt').disabled = true;
                            }

                            function validate() {
                                radioChoices = document.getElementsByName("uw_type");
                                for (x = 0; x < 3; x++)
                                    if (radioChoices[x].checked) break;
                                if (x == 0) {
                                    document.getElementById("myForm").submit();
                                    return;
                                }
                                if (x == 1) {
                                    console.log(document.getElementById("otg_amt").value);
                                    if (!document.getElementById("otg_amt").value) {
                                        alert("Please enter a valid amount1");
                                    } else {
                                        document.getElementById("myForm").submit();
                                        return;
                                    }
                                } else if (x == 2)
                                    if (!document.getElementById("other_amt").value) {
                                        alert("Please enter a valid amount2");
                                    }
                                else {
                                    document.getElementById("myForm").submit();
                                }
                            }
                    </script>

                    <tr>
                        <td><input type="radio" onclick="disableOther()" name="uw_type" value="F" checked> My Fair Share <input type="number" name="f_amt" id="f_amt" value="<?php echo $pay ?>"></td>
                        <td>(5% of gross amount per check)</td>
                    </tr>
                    <tr>
                        <td><input type="radio" onclick="disable(document.getElementById('otg_amt'), document.getElementById('other_amt'))" name="uw_type" value="O">One-time gift (amount : <input type="number" name="otg_amt" id="otg_amt" disabled>)</td>
                        <td>(Amount to be deducted from first payroll period)</td>
                    </tr>
                    <tr>
                        <td><input type="radio" onclick="disable(document.getElementById('other_amt'), document.getElementById('otg_amt'))" name="uw_type" value="T">Other amount (amount : <input type="number" name="other_amt" id="other_amt" disabled>)</td>
                        <td>(Amount to be deducted from each payroll period)</td>
                    </tr>
                </table>
                <br><br>
                <div class="credetials">
                    <table class="signature-date">
                        <tr>
                            <td><?php echo (new \DateTime())->format('Y-m-d');?></td>
                        </tr>
                        <tr>
                            <td>Date</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
                </form>
                <table>
                    <tr>
                        <td><a href="landing_user.php"><button>Opt out</button></a></td>
                        <td><button onclick="validate()">Save</button></td>
                    </tr>
                </table>
    </div>
</body>
</html>