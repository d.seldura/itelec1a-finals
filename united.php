<?php
session_start();
require_once('config.php');
date_default_timezone_set ("Asia/Manila");
$sql = "SELECT * FROM employee where username=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$_SESSION['username']]);
$result = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM job_specifics where jobID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_job_specifics']]);
$result_job = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM employee where employeeID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result_job['fk_dept_manager']]);
$result_manager = $statement->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM pay_details where payID=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_pay_details']]);
$result_pay = $statement->fetch(PDO::FETCH_ASSOC);
$pay = $result_pay['hourly_rate']*$result_pay['scheduled_hours']*4;

$sql = "SELECT * FROM deductions_united_way where united_way_id=?";
$statement = $dbConn->prepare($sql);
$statement->execute([$result['fk_deduction_uw']]);
$result_deduction_uw = $statement->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
    <link rel="stylesheet" href="./css/united.css">
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <div class="header">
                <h1>A-1 Information Systems</h1>
                <h3>Employee United Way Contribution Form</h3>
            </div>
            <div class="employee-info">
                <table class="info-table">
                    <tr>
                        <td>Employee ID: </td>
                        <td><?php echo $result['employeeID'] ?></td>
                        <td>Employee Dept: </td>
                        <td><?php echo $result_job['department'] ?></td>
                    </tr>
                    <tr>
                        <td>Employee Name: </td>
                        <td><?php echo $result['name_last'].", ".$result['name_first']." ".$result['name_middle']?></td>
                        <td>Employee Manager: </td>
                        <td><?php echo $result_manager['name_last'].", ".$result_manager['name_first']." ".$result_manager['name_middle']?></td>
                    </tr>
                </table>
            </div>
            <div class="label">
                <h3>My contribution for the calendar year  of <?php echo $result_deduction_uw['creation_date'][0].$result_deduction_uw['creation_date'][1].$result_deduction_uw['creation_date'][2].$result_deduction_uw['creation_date'][3]?> is: </h3>
            </div>
            <div class="interactive-form">
                <div class="deductiontype">
                <label>Deduction Type</label>
                <?php
                    if ($result_deduction_uw['type'] == 'F')
                        echo "<h1>Fair Share</h1>";
                    elseif ($result_deduction_uw['type'] == 'O')
                        echo "<h1>One-Time Gift</h1>";
                    elseif ($result_deduction_uw['type'] == 'T')
                        echo "<h1>Other Amount</h1>";
                    else
                        echo "<h1>No Contribution</h1>"
                ?>
                </div>
                <div class="deductionamt">
                    <label>Deduction Amount</label>
                    <h1>$<?php 
                    if ($result_deduction_uw['deduction_amount']== null)
                       echo '0';
                    else
                        echo $result_deduction_uw['deduction_amount'] ;
                    ?></h1>
                </div>
                <br><br>
                <div class="credetials">
                    <table class="signature-date">
                        <tr>
                            <td><?php echo (new \DateTime())->format('Y-m-d');?></td>
                        </tr>
                        <tr>
                            <td>Date</td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="./landing_user.php"><button>Home</button></a>
        </div>
    </div>
</body>
</html>