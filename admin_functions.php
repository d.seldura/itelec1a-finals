<?php
session_start();
require_once('config.php');

$result = $dbConn->query("SELECT * FROM employee ORDER BY employeeID ASC;");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A-1 Information Systems</title>
</head>

<body>
    <div class="container">
       <div class="emp-info">
           <a href="http://localhost/generate_account.html">
                <button>
                    Generate Account
                </button>
            </a>
            <br><br>
            <a href="http://localhost/landing_admin.html">
                <button>
                    Back
                </button>
            </a>
        </div>
    </div>
    <div class="container">
        <table>
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>MI</th>
            <th>Birthdate</th>
            <th>Work Phone</th>
            <th>Home Phone</th>
            
        <?php
        while ($row = $result->fetch(PDO::FETCH_ASSOC)){
            echo "<tr>";
            echo "<td>".$row["employeeID"]."</td>";
            echo "<td>".$row["name_last"]."</td>";
            echo "<td>".$row["name_first"]."</td>";
            echo "<td>".$row["name_middle"]."</td>";
            echo "<td>".$row["date_birth"]."</td>";
            echo "<td>".$row["contact_work_phone"]."</td>";
            echo "<td>".$row["contact_phone"]."</td>";
            echo "<td><a href=\"crud_edit_user.php?employeeID=$row[employeeID]\">Edit</a> | <a href=\"crud_view_user.php?employeeID=$row[employeeID]\" >View More</a></td></tr>";
        }
        ?>
        </table>
    </div>
</body>

</html>