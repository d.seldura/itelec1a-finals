<?php
session_start();
require_once('config.php');
$passwordGen = mt_rand(10000000,99999999);
$userpassword = password_hash($passwordGen, PASSWORD_BCRYPT);

$statement = $dbConn->prepare("INSERT INTO work_site VALUES(null,?,?,?,?)");
$result = $statement->execute([$_POST['workSite'],$_POST['workBuilding'],$_POST['workRoom'],$_POST['workMailStop']]);
$last_id_work = $dbConn->lastInsertId();

$statement = $dbConn->prepare("INSERT INTO pay_details (hourly_rate,annual_rate,scheduled_hours) VALUES(?,?,?)");
$result = $statement->execute([$_POST['payHourlyRate'],$_POST['payAnnualSalary'],$_POST['payScheduledHours']]);
$last_id_pay = $dbConn->lastInsertId();

$statement = $dbConn->prepare("INSERT INTO job_specifics (title, department,fk_dept_manager, fk_work_site) VALUES(?,?,?,?)");
$result = $statement->execute([$_POST['jobTitle'],$_POST['department'],$_POST['deptManager'],$last_id_work]);
$last_id_job = $dbConn->lastInsertId();

$usernamegen = strtolower($_POST['emp_name_first'][0].$_POST['emp_name_last'].mt_rand(10000,99999));

$statement = $dbConn->prepare("INSERT INTO employee (name_first,name_middle,name_last,employment_status,fk_job_specifics,auth_password,initial_login,is_admin,contact_work_phone) VALUES (?,?,?,?,?,?,?,?,?)");
$result = $statement->execute([$_POST['emp_name_first'],$_POST['emp_name_mi'],$_POST['emp_name_last'],$_POST['empStatus'],$last_id_job,$userpassword,true,false,"101".mt_rand(1111111,9999999)]);
$last_id = $dbConn->lastInsertId();

$statement = $dbConn->prepare("INSERT INTO activity_log (author_id,recipient_id,activity_code,reason) VALUES (?,?,?,?)");
$result = $statement->execute([$_SESSION['employeeID'],$last_id,$_POST['empActivityCode'],$_POST['empActivityReason']]);
$last_id_log = $dbConn->lastInsertId();

$usernamegen = $usernamegen.$last_id;
$statement = $dbConn->prepare("UPDATE employee SET username = ?, fk_latest_emp_activity = ?, fk_pay_details = ? WHERE employeeID=$last_id");
$result = $statement->execute([$usernamegen,$last_id_log,$last_id_pay]);



echo "Username: ".$usernamegen."<br>"."Password: ".$passwordGen."<br><br><a href=\"http://localhost/admin_functions.html\"><button>Back</button></a>";
